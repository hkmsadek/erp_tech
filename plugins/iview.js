import Vue from 'vue'
import iView from 'iview'
import ViewUI from 'view-design';
import _ from 'lodash';
// import 'view-design/dist/styles/iview.css';

import locale from 'view-design/dist/locale/en-US' // Change locale, check node_modules/iview/dist/locale
// Vue.use(ViewUI);

// import Antd from 'ant-design-vue';
// import 'ant-design-vue/dist/antd.css';
// Vue.use(Antd);
Vue.use(ViewUI, {locale});
import moment from 'moment'
Vue.use(moment);





Vue.use(iView, {
  locale
})

import { mapActions, mapGetters } from 'vuex';
// mixins for using common methods

Vue.mixin({
    filters:{
        fastAndLast(user){
            let s = user.firstname.substring(0,1)+user.lastname.substring(0,1);
            return s.toUpperCase();
        }
      },
    computed: {
        ...mapGetters({
            authUser:'getAuthUser',
            isLoggedIn:'isLoggedIn',
            projectPendingList:'getProjectPendingList',
            taskPendingList:'getTaskPendingList',
            unseenNotification:'getUnseenNotification',
            apiUrl: 'getApiUrl',
            wsUrl: 'getWsUrl',
            isSidebar: 'getSidebar',
            unseenConversation: "messenger/unseenConversation",
        }),
    },
  methods: {
      i(msg, i = 'Hey!') {
          this.$Notice.info({
              title: i,
              desc: msg
          });
      },
      s(msg, i = 'Great!') {
          this.$Notice.success({
              title: i,
              desc: msg
          });
      },
      w(msg, i = 'Hi!') {
          this.$Notice.warning({
              title: i,
              desc: msg
          });
      },
      e(msg, i = 'Oops!') {
          this.$Notice.error({
              title: i,
              desc: msg,

          });
      },
      swr() {
          this.$Notice.error({
              title: 'Oops',
              desc: 'Something went wrong, please try again later'
          });
      },
      async callApi(method, url, dataObj) {
          try {
              let data = await this.$axios({
                  config: {
                    withCredentials: true
                  },
                  method: method,
                  url: url,
                  data: dataObj,

              })
              return data

          } catch (e) {

              return e.response
          }
      },
      getToday(){
        let d = new Date();
        let monthNumber = d.getMonth() + 1;
        monthNumber = ("0" + monthNumber).slice(-2);
        let dayNumber = d.getDate();
        dayNumber = ("0" + dayNumber).slice(-2);
        let today = `${d.getFullYear()}-${monthNumber}-${dayNumber}`
        return today;
      },
  }

})

