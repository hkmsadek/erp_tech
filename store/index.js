//import Vuex from 'vuex'
//import axios from 'axios'
export const strict = false
export const state = () => ({
    authUser: false,
    projectPendingList : 0, 
    taskPendingList : 0, 
    projectRunningList : 0, 
    unseenNotification : 0, 
    apiUrl: process.env.VUE_APP_BASE_URL,
    wsUrl: process.env.VUE_APP_WS_URL,
    isSidebar:false,
})
// common getters
export const getters ={
  isLoggedIn (state) {
    return !!state.authUser
  },
  getAuthUser (state) {
    return state.authUser
  },
  getProjectPendingList (state) {
    return state.projectPendingList
  },
  getTaskPendingList (state) {
    return state.taskPendingList
  },
  getProjectRunningList (state) {
    return state.projectRunningList
  },
  getUnseenNotification (state) {
    return state.unseenNotification
  },
  getApiUrl (state) {
    return state.apiUrl;
  },
  getWsUrl (state) {
    return state.wsUrl;
  },
  getSidebar (state) {
    return state.isSidebar;
  },
}
//mutations for changing data from action
export const mutations = {
  loginUser(state, data) {
    state.authUser = data
  },
  setProPendingList(state, data){
    state.projectPendingList = data;
  },
  setTaskPendingList(state, data){
    state.taskPendingList = data;
  },
  setProRunningList(state, data){
    state.projectRunningList = data;
  },
  SetNotification(state, data){
    state.unseenNotification = data;
  },
  IncreaseNotification(state, data){
    state.unseenNotification += 1;
  },
  setSidebar (state, data) {
    state.isSidebar = data;
  },
}
// actionns for commiting mutations
export const actions = {
  async nuxtServerInit({ commit }, { $axios }) {
   console.log('I am running as nuxt server init')

    //console.log('session is ', request.session)
    try {
      // get the initial data
      let { data } = await $axios.get('/tech/initdata')
      commit('loginUser', data.user)
      //console.log(data)

    } catch (e) {
        console.log('nuxt server error ', e.response)
    }
  },
}
