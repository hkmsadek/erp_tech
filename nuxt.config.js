
export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title:'Tech-ERP-Homboltech',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/images/icon.jpeg' },
      { rel: 'stylesheet', href: "/css/bootstrap.css" },
      { rel: 'stylesheet', href: "/css/common.css" },
    ],
    script: [
      {
        src: 'https://code.jquery.com/jquery-3.4.0.min.js',
        integrity: 'sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg=',
        crossorigin: "anonymous"
      },
      { src: "/js/poly.js", body: true },
      { src: "/js/adonis.js", body: true },
      { src: "/js/external_api.js", body: true },
      { src: "https://www.google.com/recaptcha/api.js", body: true }
    ]

  },
  /*
  ** Global CSS
  */
  css: [
    'iview/dist/styles/iview.css',
    '@/assets/css/main.css',
    '@/assets/css/responsive.css'
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    '@/plugins/iview'
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/moment'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
 axios: {
  baseURL: process.env.BASE_URL,
  credentials: true
},
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
  },
  server: {
    // port: 8000, // default: 3000
    port: 8084, // default: 3000
    host: 'localhost' // default: localhost
  }

}
